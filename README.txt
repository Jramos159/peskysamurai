Group Name;
    Pesky Samurai

Member Names:
    Teddy Koniarz
    Justin Ramos

Game Name:
    King of the Hill

Genre:
    Side-scroller

Objective:
    Get to the hill (goal)

Features:
    Jumping
    Timer

Controls:
    (AD/Arrow Keys)- movement
    (W/Space) - jump

Simple level showing major mechanics game will contain.