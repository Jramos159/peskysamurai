﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour {

    public Timer _Timer;
    
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "killzone")
        {
            Debug.Log("Collision");
            Destroy(gameObject);
        }

    }

    void OnCollisionCollider2D(Collider2D col)
    {
        if (col.gameObject.tag == "Finish")
        {
            Debug.Log("Win");
            _Timer.Win();
        }
    }
}
