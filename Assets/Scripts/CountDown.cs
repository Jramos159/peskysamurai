﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class CountDown: MonoBehaviour
{

    public Text timer = null;
    public Text Text = null;
    public Canvas WLScreen;
    public Camera cam1;
    private float time = 20f;

    // Use this for initialization
    void Start()
    {
        Text.enabled = false;
        cam1.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTime();
        if (transform.position.x >= 6.75 && transform.position.x <= 7.25)
        {
            Win();
        }
    }

    void UpdateTime()
    {
        time -= Time.deltaTime;
        if (time >= 0.0f)
        {
            timer.text = ("Time: " + System.Math.Round(time, 1) + " seconds");
        }

        if (time <= 0.1f)
        {
            Lose();
        }
    }

    void Lose()
    {
        cam1.enabled = true;
        Destroy(gameObject);

        Text.text = ("You Lose");
        Text.enabled = true;
    }

    void Win()
    {
        cam1.enabled = true;
        Destroy(gameObject);

        Text.text = ("You Win");
        Text.enabled = true;
    }
}
