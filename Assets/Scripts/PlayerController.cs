﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float speed = 5f;
    public float maxSpeed = 3f;
    public Text timer = null;
    public Text Text = null;
    public Canvas WLScreen;
    public Camera cam1;
    private float time = 20f;

    private Rigidbody2D rb2d;

    void Start()
    {
        Text.enabled = false;
        cam1.enabled = false;

        rb2d = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        transform.Translate(Vector2.right * speed * Time.deltaTime * h);

        UpdateTime();
        if (transform.position.x >= 6.75 && transform.position.x <= 7.25)
        {
            Win();
        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "killzone")
        {
            Debug.Log("Collision");
            transform.position = new Vector2(0, 1.5f);
        }
    }

    void OnCollisionCollider2D(Collider2D col)
    {
        if (col.gameObject.tag == "Finish")
        {
            Debug.Log("Win");
        }
    }

    void UpdateTime()
    {
        time -= Time.deltaTime;
        if (time >= 0.0f)
        {
            timer.text = ("Time: " + System.Math.Round(time, 1) + " seconds");
        }

        if (time <= 0.1f)
        {
            Lose();
        }
    }

    void Lose()
    {
        cam1.enabled = true;
        Destroy(gameObject);

        Text.text = ("You Lose");
        Text.enabled = true;
    }

    void Win()
    {
        cam1.enabled = true;
        Destroy(gameObject);

        Text.text = ("You Win");
        Text.enabled = true;
    }
}
