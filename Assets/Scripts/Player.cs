﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour {

    public float speed = 5f;
    public float jumpForce = 100;
    public float time = 15f;
    public float finalTime;
    public float startingTime;

    public bool canMove = true;
    public bool hittingWall = false;
    public bool updateTime = true;
    public bool grounded = true;

    public Text timer = null;
    public Text Text = null;
    public Camera PlayerCam;
    public Camera WLCam;
    private Rigidbody2D rb2d;
    private AudioSource jump;

	void Start () {
        Text.enabled = false;
        WLCam.enabled = false;
        startingTime = time;

        rb2d = gameObject.GetComponent<Rigidbody2D>();
        jump = gameObject.GetComponent<AudioSource>();
	}
	
	void Update () {
        RaycastHit hit;
        Ray hitWall = new Ray(transform.position, Vector2.right);

        if (canMove)
        {
            float h = Input.GetAxis("Horizontal");
            if (Physics.Raycast(hitWall, out hit, speed * Time.deltaTime))
            {
                if (hit.collider.tag == "wall")
                {
                    print("Cant move");
                }
            }
            else
            {
                transform.Translate(Vector2.right * speed * Time.deltaTime * h);
            }
        }

        if (canMove && grounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }

        }

        if (updateTime)
        {
            UpdateTime();
        }


	}

    //Update Time
    void UpdateTime()
    {
        time -= Time.deltaTime;
        if(time >= 0.0f)
        {
            timer.text = ("Time: " + System.Math.Round(time, 1) + " seconds");
        }

        //If time ends player loses
        if (time <= 0.1f)
        {
            timer.text = ("Time: 0.0 seconds");
            Lose();
        }
    }



    void Jump()
    {
        jump.Play();
        grounded = false;
        print("Jumping");
        rb2d.AddForce(new Vector2(0,jumpForce));

    }
    //Loses
    void Lose()
    {
        canMove = false;
        updateTime = false;
        PlayerCam.enabled = false;
        WLCam.enabled = true;

        timer.fontSize = 2;
        Text.text = ("You Lose");
        Text.enabled = true;
    }

    //Wins
    void Win()
    {
        canMove = false;
        updateTime = false;
        PlayerCam.enabled = false;
        WLCam.enabled = true;

        Text.text = ("You Win");
        Text.enabled = true;
        finalTime = startingTime - time;
        timer.fontSize = 2;
        timer.text = ("Your time: " + System.Math.Round(finalTime, 1) + " seconds");
    }



    //Checks for collisions
    void OnTriggerEnter2D(Collider2D col)
    {
        //Falls off edge
        if (col.gameObject.tag == "killzone")
        {
            print("Respawn");
            transform.position = new Vector2(-7,.75f);
        }

        //Wins
        if (col.gameObject.tag == "goal")
        {
            print("Win");
            Win();
        }

        //Lands on Ground
        if (col.gameObject.tag == "ground")
        {
            grounded = true;
        }

        //Against wall
        if (col.gameObject.tag == "wall")
        {
            rb2d.velocity = Vector2.zero;
        }
    }

}
