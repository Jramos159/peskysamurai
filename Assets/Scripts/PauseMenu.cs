﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PauseMenu : MonoBehaviour {

    public bool paused = false;

    public Canvas pauseMenu;
    public Canvas quitMenu;
    public Button resumeText;
    public Button restartText;
    public Button exitText;

    
    // Use this for initialization
	void Start () {
        pauseMenu = pauseMenu.GetComponent<Canvas>();
        quitMenu = quitMenu.GetComponent<Canvas>();
        resumeText = resumeText.GetComponent<Button>();
        restartText = restartText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        pauseMenu.enabled = false;
        quitMenu.enabled = false;
    }

    public void Paused()
    {
        pauseMenu.enabled = true;
    }

    public void Resume()
    {
        pauseMenu.enabled = false;
        GameObject.Find("Body").GetComponent<PlayerController>().paused = false;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitPress()
    {
        quitMenu.enabled = true;
        resumeText.enabled = false;
        restartText.enabled = false;
        exitText.enabled = false;
    }

    public void NoPress()
    {
        quitMenu.enabled = false;
        resumeText.enabled = true;
        restartText.enabled = true;
        exitText.enabled = true;
    }

    public void ExitGame()
    {
        Application.Quit();
    }


	
	// Update is called once per frame
	void Update () {
        paused = GameObject.Find("Body").GetComponent<PlayerController>().paused;

        if (paused)
        {
            Paused();
        }
	}
}
