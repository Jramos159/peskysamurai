﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class WorldMenu : MonoBehaviour
{

    public Canvas worldMenu;
    public Button Level_1;

    void Start()
    {
        Level_1 = Level_1.GetComponent<Button>();
    }

    public void StartLevel()
    {
        SceneManager.LoadScene(2);
    }
}
