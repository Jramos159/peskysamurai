﻿using UnityEngine;
using System.Collections;

public class KingScript: MonoBehaviour
{
    public bool kingDown = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "mudpit")
        {
            print("King has fallen");
            kingDown = true;
        }
    }
}
