﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public float speed = 5f;
    public float jumpForce = 100;
    public float time = 0f;
    public int countDown = 3;
    private float delay = 3f;
    

    public bool canMove = true;
    public bool updateTime = true;
    public bool grounded = true;
    public bool kingDown = false;
    public bool paused = false;
    public bool win = false;

    public Text timer = null;
    public Canvas MainCanvas = null;
    private Rigidbody2D rb2d;
    private AudioSource jump;
    public float startingX;
    public float startingY;

    void Start() {

        rb2d = gameObject.GetComponent<Rigidbody2D>();
        jump = gameObject.GetComponent<AudioSource>();
        startingX = transform.position.x;
        startingY = transform.position.y;

    }


    void Update() {

        if (!paused && !win)
        {
            canMove = true;
            updateTime = true;
            rb2d.isKinematic = false;
        }

        kingDown = GameObject.Find("King").GetComponent<KingScript>().kingDown;

        if (canMove)
        {
            float h = Input.GetAxis("Horizontal");
            transform.Translate(Vector2.right * speed * Time.deltaTime * h);
        }

        if (canMove && grounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }
        }

        if (updateTime)
        {
            UpdateTime();
        }

        if (Input.GetButtonDown("Cancel"))
        {
            paused = true;
            canMove = false;
            updateTime = false;
            rb2d.isKinematic = true;
        }
    }

    void UpdateTime()
    {
        while(countDown > 0)
        {
            countDown -= (int)Time.deltaTime;
            timer.text = (countDown + "");
        }
        time += Time.deltaTime;
        timer.text = ("Time: " + System.Math.Round(time, 1) + " seconds");

    }

    void Jump()
    {
        jump.Play();
        grounded = false;
        print("Jumping");
        rb2d.AddForce(new Vector2(0, jumpForce));
    }

    void Win()
    {
        canMove = false;
        updateTime = false;
        win = true;
        DelayEnd();
    }



    void DelayEnd()
    {
        delay -= Time.deltaTime;
        if(delay <= 0.1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Respawn")
        {
            print("Respawn");
            transform.position = new Vector2(startingX, startingY);
        }

        if (col.gameObject.tag == "goal" && kingDown)
        {
            print("Win");
            Win();
        }

        if (col.gameObject.tag == "ground")
        {
            grounded = true;
        }
    }
}
